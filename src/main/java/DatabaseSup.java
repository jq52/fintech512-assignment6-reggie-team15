import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

public class DatabaseSup {
    private static String dbName="reggie";
    private static String connectionURL = "jdbc:derby:" + dbName + ";create=true";
    private static Connection conn = null;

    public static Connection getConnect() throws SQLException {
        if (conn == null || conn.isClosed()) {
            conn = DriverManager.getConnection(connectionURL);
        }
        return conn;
    }

    public static void closeConnection(){
        try {
            conn.close();
        } catch (Exception ignored) {
        }
    }

    public static Course createCourse(String name, int credits) throws Exception {
        Connection conn = getConnect();
        try {
            Statement statement = conn.createStatement();
            statement.executeUpdate("DELETE FROM course WHERE name = '" + name + "'");
            statement.executeUpdate("INSERT INTO course VALUES ('" + name + "', " + credits
                    + ")");
            return new Course(name, credits);
        } finally {
            closeConnection();
        }
    }

    public static Course findCourse(String name) throws Exception {
        Connection conn = getConnect();
        try {
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM course WHERE name = '" + name
                    + "'");
            if (!result.next())
                return null;
            int credits = result.getInt("Credits");
            return new Course(name, credits);
        } finally {
            closeConnection();
        }
    }

    public static void updateCourse(Course up_course) throws Exception{
        Connection conn = getConnect();
        try {
            Statement statement = conn.createStatement();
            statement.executeUpdate("DELETE FROM course WHERE name = '" + up_course.getName() + "'");
            statement.executeUpdate("INSERT INTO course VALUES('" + up_course.getName() + "'," + up_course.getCredits() + ")");
        } finally {
            closeConnection();
        }
    }

    public static Offering createOffering(Course course, String daysTimesCsv) throws Exception{
        Connection conn = getConnect();
        try {
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery("SELECT MAX(id) FROM offering");
            result.next();
            int newId = 1 + result.getInt(1);

            statement.executeUpdate("INSERT INTO offering VALUES (" + newId + ",'"
                    + course.getName() + "','" + daysTimesCsv + "')");
            return new Offering(newId, course, daysTimesCsv);
        } finally {
            closeConnection();
        }
    }

    public static Offering findOffering(int id) throws Exception {
        Connection conn = getConnect();

        try {
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM offering WHERE id =" + id
                    + "");
            if (result.next() == false)
                return null;

            String courseName = result.getString("name");
            Course course = findCourse(courseName);
            String dateTime = result.getString("daysTimes");
            return new Offering(id, course, dateTime);
        } catch (Exception ex) {
            return null;
        } finally{
            closeConnection();
        }
    }

    public static void updateOffering(Offering up_offering) throws Exception {
        Connection conn = getConnect();
        try {
            Statement statement = conn.createStatement();
            statement.executeUpdate("DELETE FROM offering WHERE id=" + up_offering.getId() + "");
            statement.executeUpdate("INSERT INTO offering VALUES(" + up_offering.getId() + ",'" + up_offering.getCourse().getName()
                    + "','" + up_offering.getDaysTimes() + "')");
        } finally {
            closeConnection();
        }
    }

    public static void deleteAll() throws Exception {
        Connection conn = getConnect();

        try {
            Statement statement = conn.createStatement();

            statement.executeUpdate("DELETE FROM schedule");
        } finally {
            closeConnection();
        }
    }

    public static Schedule createSchedule(String name) throws Exception {
        Connection conn = getConnect();

        try {
            Statement statement = conn.createStatement();

            statement.executeUpdate("DELETE FROM schedule WHERE name = '" + name + "'");
            return new Schedule(name);
        } finally {
            closeConnection();
        }
    }

    public static Schedule findSchedule(Schedule schedule) throws Exception {
        Connection conn = getConnect();
        try {
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery(
                    "SELECT * FROM schedule WHERE name= '" + schedule.name + "'");
            conn.setAutoCommit(false);
            while (result.next()) {
                int offeringId = result.getInt("offeringId");
                Offering offering = Offering.find(offeringId);
                if (offering != null) {
                    schedule.add(offering);
                }
            }
            return schedule;
        } finally {
            closeConnection();
        }
    }

    public static void updateSchedule(Schedule up_schedule) throws Exception {
        Connection conn = getConnect();
        try {
            Statement statement = conn.createStatement();
            statement.executeUpdate("DELETE FROM schedule WHERE name = '" + up_schedule.name + "'");
            for (int i = 0; i < up_schedule.schedule.size(); i++) {
                Offering offering = (Offering) up_schedule.schedule.get(i);
                int resultCount = statement.executeUpdate("INSERT INTO schedule VALUES('" + up_schedule.name + "',"
                        + offering.getId() + ")");
            }
        } finally {
            closeConnection();
        }
    }

    public static Collection<Schedule> allSchedule() throws Exception {
        ArrayList<Schedule> result = new ArrayList<Schedule>();
        Connection conn = getConnect();

        try {
            Statement statement = conn.createStatement();
            ResultSet results = statement.executeQuery("SELECT DISTINCT name FROM schedule ORDER BY name");

            while (results.next())
                result.add(Schedule.find(results.getString("name")));
        } finally {
            closeConnection();
        }

        return result;
    }

}

